import React from 'react';
import ReactDOM from 'react-dom';
import Api from './Api';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Api />, div);
  ReactDOM.unmountComponentAtNode(div);
});
