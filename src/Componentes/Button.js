import React from 'react';
import Modal from 'react-modal';
import Api from '../Api/ApiEstatica';

export class ButtonAction extends React.Component{
  
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false,coments:[] };
     }
  
  openModal = () => {
    this.setState({modalIsOpen: true});
   }
 
  handleModalCloseRequest = () => {
    this.setState({modalIsOpen: false});
   }
    
  handleSaveClicked = (e) => {
    alert('Save button was clicked');
   }

  render (){
    return(
      <div>
      <button type="button" className="btn" onClick={this.openModal}>Abrir Comentarios</button>
      <Modal
      className="Modal__Bootstrap modal-dialog"
      closeTimeoutMS={150}
      isOpen={this.state.modalIsOpen}
      onRequestClose={this.handleModalCloseRequest}
      >
      <div className="modal-content">
        <div className="modal-header">                                
          <button type="button" className="close" onClick={this.handleModalCloseRequest}>
          <span aria-hidden="true">&times;</span>
          <span className="sr-only">Close</span>
          </button>
        </div>
          <div className="modal-body">   
            <div className="title">
              Correo:{Api.posts.email}                    
            </div>
            <br/>
            <div className="modalbody_body">
             Contenido: {Api.posts.contenidoEmail}                 
            </div>
            <br/>
            <img className="imagen_body" src= "https://via.placeholder.com/150/ecde"></img>         
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-secondary" onClick={this.handleModalCloseRequest}>Close</button>                 
          </div>
      </div>
      </Modal>
    </div>
    )
  }
}

export default ButtonAction;