import React from 'react';
import '../App.css';
import Api from '../Api/ApiEstatica';
import ButtonAction from './Button';
export class Post extends React.Component{

  render(){    
   console.log(Api.posts.titulo)
    return(
      <div className="col-md-8">
        <div >                    
          <div className="card-body">
            <h1>Titulo: {Api.posts.titulo} </h1>
            <h2> Contenido: {Api.posts.cuerpo}</h2>
            <ButtonAction/>
          </div>                    
        </div>                
      </div>               
    )
  }
}
export default Post;